<?php
include_once('master_patch.php');
$query = "SELECT * FROM cars WHERE id = ".$_GET['id'];

$result = mysqli_query($conn, $query);
$data = mysqli_fetch_assoc($result);

?>


<?php include_once('header.php');?>

                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Update Content</div>
                    <div class="panel-body">
                        <form action="update.php?id=<?php echo $_GET['id'];?>" method="POST" >

                            <div class="form-group">
                                <label name="brand">Brand Name</label>
                                <input type="text" class="form-control" name="brand" placeholder="Brand Name" value="<?php echo $data['brand']?>"/>
                                <input type="hidden" class="form-control" name="id" placeholder="Brand Name" value="<?php echo $data['id']?>"/>
                            </div>

                            <div class="form-group">
                                <label name="color">Color</label>
                                <input type="text" class="form-control" name="color" placeholder="Color" value="<?php echo $data['color']?>"/>
                            </div>

                            <div class="form-group">
                                <label name="color">Select Your Color:</label><br/>
                                <div class="radio-inline">
                                    <label><input type="radio" name="type" value="Car" <?php if($data['type']== "Car"){ echo "checked =checked";}?>>Car</label>
                                </div>
                                <div class="radio-inline">
                                    <label><input type="radio" name="type" value="Jeep" <?php if($data['type']== "Jeep"){ echo "checked =checked";}?>>Jeep</label>
                                </div>
                                <div class="radio-inline">
                                    <label><input type="radio" name="type" value="Pickup" <?php if($data['type']== "Pickup"){ echo "checked =checked";}?>>Pickup</label>
                                </div>
                                <div class="radio-inline">
                                    <label><input type="radio" name="type" value="Bus" <?php if($data['type']== "Bus"){ echo "checked =checked";}?>>Bus</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label name="licence">Licence</label>
                                <input type="text" class="form-control" name="licence"placeholder="Licence No." value="<?php echo $data['licence']?>"/>
                            </div>
                            <div>
                                <button class="btn btn-success" name="btn" type="submit">Update</button>
                                <button class="btn btn-info" type="reset">Reset</button>
                                <button class="btn btn-primary" type="button">Click Me</button>
                            </div>
                        </form>
                    </div>
                </div>

<?php include_once('footer.php')?>