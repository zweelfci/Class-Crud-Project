<?php

// Create connection
include_once('master_patch.php');
$fdata = get_all_cars();
//echo "<pre>";
//print_r($fdata);
//exit();

?>
<?php include_once('header.php') ?>
            <?php
            if(array_key_exists('message',$_GET)){
            ?>
                <div class="alert alert-info"><?php echo $_GET['message'];?></div>
            <?php
            }
            ?>
            <div class="panel panel-default" >
                <!-- Default panel contents -->
                <div class="panel-heading">All Content</div>
                <div class="panel-body">
                    <div class="" style="padding-bottom: 30px">
                        <nav>
                            <li class="btn btn-success btn-sm"><a href="create.php"><span class="glyphicon glyphicon-plus"></span> Add Cars</a></li>
                            <li class="btn btn-default btn-sm"><a href="excel.html">Download Excel</a></li>
                            <li class="btn btn-default btn-sm"><a href="pdf.html">Download PDF</a></li>
                        </nav>
                    </div>

                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>SL</th>
                            <th>Brand</th>
                            <th>Color</th>
                            <th>Type</th>
                            <th>Licence</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        foreach($fdata as $data) {
                         ?>
                            <tr>
                                <td><?php echo $data['id'];?></td>
                                <td><?php echo $data['brand'];?></td>
                                <td><?php echo $data['color'];?></td>
                                <td><?php echo $data['type'];?></td>
                                <td><?php echo $data['licence'];?></td>
                                <td><a href="show.php?id=<?php echo $data['id'];?>"><span class="glyphicon glyphicon-eye-open"></span></a> | <a href="edit.php?id=<?php echo $data['id'];?>"><span class="glyphicon glyphicon-pencil"></span></a> | <a href="delete.php?id=<?php echo $data['id'];?>"><span class="glyphicon glyphicon-remove"></span></a> | <a href=""><span class="glyphicon glyphicon-trash"></span></a></td>
                            </tr>
                        <?php } ?>


                        </tbody>
                    </table>
                </div>
            </div>

<?php include_once('footer.php')?>