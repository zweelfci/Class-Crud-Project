<?php include_once('header.php');?>
                <div class="panel panel-default" >
                    <!-- Default panel contents -->
                    <div class="panel-heading">Add Content</div>
                    <div class="panel-body">
                        <form action="store.php" method="POST">

                            <div class="form-group">
                                <label name="brand">Brand Name</label>
                                <input type="text" class="form-control" name="brand" placeholder="Brand Name" value=""/>
                            </div>

                            <div class="form-group">
                                <label name="color">Color</label>
                                <input type="text" class="form-control" name="color" placeholder="Color" value=""/>
                            </div>

                            <div class="form-group">
                                <label name="color">Select Your Color:</label><br/>
                                <div class="radio-inline">
                                    <label><input type="radio" name="type" value="Car">Car</label>
                                </div>
                                <div class="radio-inline">
                                    <label><input type="radio" name="type" value="Jeep">Jeep</label>
                                </div>
                                <div class="radio-inline">
                                    <label><input type="radio" name="type" value="Pickup">Pickup</label>
                                </div>
                                <div class="radio-inline">
                                    <label><input type="radio" name="type" value="Bus">Bus</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label name="licence">Licence</label>
                                <input type="text" class="form-control" name="licence"placeholder="Licence No." value=""/>
                            </div>
                            <div>
                                <button class="btn btn-success" type="submit">Save</button>
                                <button class="btn btn-info" type="reset">Reset</button>
                                <button class="btn btn-primary" type="button">Click Me</button>
                            </div>
                        </form>
                    </div>
                </div>

<?php include_once('footer.php')?>