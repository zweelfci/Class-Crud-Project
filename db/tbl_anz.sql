-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2017 at 06:32 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tbl_anz`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(5) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `color` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  `licence` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `brand`, `color`, `type`, `licence`) VALUES
(67, 'BMW 12', 'Black ', 'Jeep', 'Dhaka-ka-1209 '),
(68, 'Tata', 'Red', 'Pickup', 'Dhaka-ga-1003'),
(69, 'Toyota', 'Whaite', 'Car', 'Dhaka-Kha-1205'),
(70, 'Toyota', 'Whaite', 'Car', 'Dhaka-Kha-1205'),
(72, 'Suzuki', 'Green', 'Car', 'Khulna-ga-0034'),
(73, 'Maruti', 'White', 'Jeep', 'Dhakaka1101'),
(74, 'Hyundai', 'Red', 'Car', 'Dhaka-ga-0000'),
(75, 'Honda', 'Golden', 'Jeep', 'Barishal-ka-1209'),
(76, 'Nissan', 'parple', 'Jeep', 'Dhaka-ha-3038'),
(77, 'Chery', 'Golden', 'Jeep', 'Dhaka-gha-3122'),
(78, 'Lexus', 'Black', 'Jeep', 'Dhaka-Cha-10198'),
(79, 'Acura', 'Black', 'Car', 'Dhaka-Fa-1110'),
(80, 'Jaguar', 'Green', 'Car', 'Dhaka-ga-1200'),
(81, 'Ford', 'Black', 'Bus', 'Dhaka-Hh-1110');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
